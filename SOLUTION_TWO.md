In this solution ticked is taken off the cell so it is no longer globally accesible by any thread
and instead an arraylist is created every time the method is called and all operations are performed using it.
Thus every thread that uses movesBetween has their own group of ticked values for cells and can't impact each others values
although this is slow it is less slow then implementing locking and results in only independence of threads so that
no thread must wait on another to complete.